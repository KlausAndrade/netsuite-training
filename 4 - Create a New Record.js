/**
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(["N/record"], function (r) {
    function createEmployee() {
        r.create({
            type: r.Type.CONTACT,
            isDynamic: false,
            defaultValues: null
        }).setValue({
            fieldId: "firstname",
            value: "Klaus",
            ignoreFieldChange: true
        }).setValue({
            fieldId: "middlename",
            value: "B"
        }).setValue({
            fieldId: "lastname",
            value: "Andrade"
        }).setValue({
            fieldId: "email",
            value: "teste@mail.com"
        }).setValue({
            fieldId: "officephone",
            value: "+16233491564"
        }).save({
            enableSourcing: true,
            ignoreMandatoryFields: false
        });
    }
    return {
        afterSubmit: createEmployee
    };
});