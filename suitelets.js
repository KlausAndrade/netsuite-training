define(["N/search", "N/ui/serverWidget", "N/url", "N/log"], function (s, ui, url, log) {

    /**
     * Example Suitelet: Display Search Results in a List
     *
     * @exports suitelet-results
     *
     * @requires N/search
     * @requires N/ui/serverWidget
     * @requires N/url
     * @requires N/log
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType Suitelet
     */
    var exports = {};

    /**
     * <code>onRequest</code> event handler
     *
     * @governance 0
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @return {void}
     *
     * @static
     * @function onRequest
     */
    function onRequest(context) {
        log.audit({title: "Request received."});

        context.response.writePage({
            pageObject: renderList(findContacts())
        });
    };

    function renderList( results ) {
        var list = ui.createList({title: "Missing Email"});

        list.addColumn({
            id: "title",
            type: ui.FieldType.TEXT,
            label: "Title"
        });
        list.addColumn({
            id: "company",
            type: ui.FieldType.TEXT,
            label: "Company"
        });
        list.addColumn({
            id: "email",
            type: ui.FieldType.TEXT,
            label: "Email"
        });
        
        list.addRows({rows: results})

        return list;
    }

    function findContacts() {
        log.audit({title: "Finding Contacts."});

        return s.create({
            type: s.Type.SUPPORT_CASE,
            filters: [
                ["email", "ISEMPTY", ""]
            ],
            columns: [
                "email",
                "title",
                "company",
            ]
        }).run().getRange({start: 0, end: 20});
    }
    exports.onRequest = onRequest;
    return exports;
});
