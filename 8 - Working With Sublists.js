/**
 * @NApiVersion 2.0
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(["N/record"], function (r) {
    function onRequest() {
        var order = r.create({
            type: r.Type.SALES_ORDER,
            isDynamic: false,
            defaultValues: {
                entity: 75
            }
        });
        
        addLine(order);
        
        var orderId = order.save({
            enableSourcing: true
        });
        order = r.load({
            type: r.Type.SALES_ORDER,
            id: orderId,
            isDynamic: true
        });
        
        updateLine(order);
        insertLine(order);
        removeLine(order);
    }
    
    function addLine(rec) {
        rec.selectNewLine({sublistId:"item"});
        rec.setCurrentSublistValue({
            sublistId: "item",
            fieldId: "item",
            value: "123"
        });
        rec.setCurrentSublistValue({
            sublistId: "item",
            fieldId: "quantity",
            value: 5
        });
        rec.commitLine({sublistId:"item"});
    }
    
    function updateLine(rec) {
        rec.selectLine({
            sublistId: "item",
            line: 0
        });
        
        var currentQuantity = parseFloat(rec.getCurrentSublistValue({
            sublistId: "item",
            fieldId: "quantity"
        }));
        
        var newQuantity = currentQuantity + 2;
        
        rec.setCurrentSublistValue({
            sublistId: "item",
            fieldId: "quantity",
            value: newQuantity
        });
        
        rec.commitLineItem({sublistId:"item"});
    }
    
    function insertLine(rec) {
        rec.insertLine({
            sublistId: "item",
            line: 0
        });
        rec.setCurrentSublistValue({
            sublistId: "item",
            fieldId: "item",
            value: "77"
        });
        rec.setCurrentSublistValue({
            sublistId: "item",
            fieldId: "quantity",
            value: 1
        });
        rec.setCurrentSublistValue({
            sublistId: "item",
            fieldId: "rate",
            value: 20
        });
        rec.commitLine({sublistId:"item"});
    }
    
    function removeLine(rec) {
        var index = rec.findSublistLineWithValue({
            sublistId: "item",
            fieldId: "item",
            value: 77
        });
        
        if (index === -1) {
            // Not found
            return;
        }
        
        rec.removeLine({
            sublistId: "item",
            line: index
        });
    }
    
    return {
        onRequest: onRequest
    };
});