    define(["N/search", "N/ui/serverWidget", "N/url", "N/log"], function (s, ui, url, log) {

        /**
         * Example Suitelet: Display Search Results in a List
         *
         * @exports suitelet-results
         *
         * @requires N/search
         * @requires N/ui/serverWidget
         * @requires N/url
         * @requires N/log
         *
         * @NApiVersion 2.x
         * @NModuleScope SameAccount
         * @NScriptType Suitelet
         */
        var exports = {};

        /**
         * <code>onRequest</code> event handler
         *
         * @governance 0
         *
         * @param context
         *        {Object}
         * @param context.request
         *        {ServerRequest} The incoming request object
         * @param context.response
         *        {ServerResponse} The outgoing response object
         *
         * @return {void}
         *
         * @static
         * @function onRequest
         */
        function onRequest(context) {
            log.audit({title: "Request received."});

            context.response.writePage({
                pageObject: renderList(translate(findContacts()))
            });
        };

        // Add the created columns and populate it
        function renderList( results ) {
            var list = ui.createList({title: "These contacts don't have email"});

            list.addColumn({
                id: "internalid",
                type: ui.FieldType.TEXT,
                label: "internalid"
            });
            list.addColumn({
                id: "title",
                type: ui.FieldType.TEXT,
                label: "Title"
            });
            list.addColumn({
                id: "company",
                type: ui.FieldType.TEXT,
                label: "Company"
            });
            list.addColumn({
                id: "email",
                type: ui.FieldType.TEXT,
                label: "Email"
            });
            
            list.addRows({rows: results})

            return list;
        }
        // Create the Columns and filter content
        function findContacts() {
            log.audit({title: "Finding Contacts."});

            return s.create({
                type: s.Type.SUPPORT_CASE,
                filters: [
                    ["email", "ISEMPTY", ""]
                ],
                columns: [
                    //FIRSTNAME NOT WORKING!!!! :(
                    "internalid",
                    "email",
                    "title",
                    "company",
                ]
            }).run().getRange({start: 0, end: 20}); // SHow only 20 results
        }

        function resultToObject(result) {
            return {
                "internalid": result.getValue({name: "internalid"}),
                "email": result.getText({name: "email"}),
                "title": result.getValue({name: "title"}),
                "company": result.getText({name: "company"})
            };
        }

        function translate(results) {
            return results.map(resultToObject);
        }

        exports.onRequest = onRequest;
        return exports;
    });
