/**
    @NApiVersion 2.0
    @NScriptType ClientScript
    @NModuleScope Public
*/

 define([], function () {
    function showMessage() {
       var message = "Hello, World";
       alert(message);        
    }

    return {
        pageInit: showMessage
    };
     
 });