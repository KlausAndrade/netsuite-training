/**
    @NApiVersion 2.0
    @NScriptType ClientScript
    @NModuleScope Public
*/

 define([], function ( data ) {
    function showMessage() {
       var message = "The email field is empty.";
       var email =  data.currentRecord.getValue({
           "fieldId":"email"
       });
    }

    return {
        pageInit: showMessage
    };
     
 });