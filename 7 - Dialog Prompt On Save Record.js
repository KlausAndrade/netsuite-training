/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/ui/dialog'],
/**
 * @param {dialog} dialog
 */
function(dialog) {
    alert('test func');
    //finalResult and finalResultSet are global variables
    //accessible by all functions

        //User provided final answer from confirm box
    var finalResult = false
	
        //Flag to indicate if user provided final answer or not
	finalResultSet = false;
	
    //Save record trigger
    function saveRecord(context) 
    {
    	//If user have never provided a final answer,
    	if (!finalResultSet)
    	{
    		//Here you do your own saveRecord validation/automation.
        	//.....
        	//.....
        	//.....
        	//During validation/automation, 
    		//	if something fails, you would return false
    		//	which will never get to below line.
    		
    		//If everything pases, show the dialog box

    		dialog.confirm({
    			'title':'SS2.0 Confirm',
    			'message':'This is SuiteScript 2.0 version'+
                                  'Do you wish to continue?'
    		}).then(success).catch(fail);
    	}
    	//If user provided a final answer from confirm box, return out
    	else
    	{
    		//Reset the finalResultSet flag to false 
    		//	in case user selected "Cancel" on the confirm box.
    		finalResultSet = false;
    		
    		//return will either give the control back to user
    		//	or continue with saving of the record
    		return finalResult;
    	}
    }

    //Helper function called when user 
    //	selects Ok or Cancel on confirm box
    function success(result)
    {
    	//Sets value of finalResult to user provided answer
    	finalResult = result;
    	//Updates the finalResultSet flag to true
    	//	to indicate that user has made his/her choice
    	finalResultSet = true;
    	
    	//getNLMultiButtonByName function is undocumented client side
    	//	function call used by NetSuite when save button is clicked
    	//	triggering this function will simulate clicking Save button
    	//	programmatically and force NetSuite to trigger saveRecord again
    	//	with global variables finalResult and finalResultSet altered!
    	
    	//NS Hack to call simulate user clicking Save Button
    	getNLMultiButtonByName('multibutton_submitter').onMainButtonClick(this);
    }
    
    function fail(reason)
    {
    	return false;
    }
    
    return {
        saveRecord: saveRecord
    };
    
});