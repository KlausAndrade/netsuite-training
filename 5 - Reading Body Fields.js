/**
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 * @NModuleScope Public
 * @appliedtorecord Employee
 */
define(["N/record"], function (r) {
    function onAfterSubmit(data) {
        var currentRecord = data.newRecord;
        var salesRepId = currentRecord.getValue({
            fieldId: "salesrep"
        });
        
        if (!salesRepId) {
            return;
        };
        
        var salesRepRec = r.load({
            type: r.Type.EMPLOYEE,
            id: salesRepId,
            isDynamic: false,
            defaultValues: null
        });
        
        var salesRepEmail = salesRepRec.getValue({
            fieldId: "email"
        });
        
        if (salesRepEmail) {
            sendAwesomeNotification(salesRepEmail);
        }
    }
    
    function sendAwesomeNotification(email) {
        // Send a super useful, actionable
        // notification to the provided email address
    }

    return {
        afterSubmit: onAfterSubmit
    };
});